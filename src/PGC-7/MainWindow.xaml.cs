﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Layout;

namespace PGC_7
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var doc = new LayoutDocument();

            doc.Title = "Close me";
            doc.Content = "Hello";
            docPane.Children.Add(doc);

            doc = new LayoutDocument();

            doc.Title = "Close me also";
            doc.Content = "Hello";
            docPane.Children.Add(doc);
        }
    }
}
