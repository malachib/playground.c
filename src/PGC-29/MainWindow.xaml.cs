﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

// Guidance from [3.1] - has some target/OS build issues currently

//using Windows.Media.Capture;
//using Windows.UI.Xaml.Controls;

namespace PGC_29
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string[] names_ = { "Marshal", "Will", "Holly" };
        int nameIndex_ = -1;
        ViewModel model;
        //readonly MediaCapture mediaCapture_ = new MediaCapture();

        public MainWindow()
        {
            var viewModel = new ViewModel();

            viewModel.FirstName = "Kevin";

            DataContext = viewModel;
            InitializeComponent();

            model = viewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (++nameIndex_ == names_.Length) nameIndex_ = 0;

            model.FirstName = names_[nameIndex_];
        }
    }
}
