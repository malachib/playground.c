using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PGC_29
{
    public class ViewModel :
        INotifyPropertyChanging,
        INotifyPropertyChanged
    {
        //public string FirstName { get; set; }
        private string? firstName_;

        public string? FirstName
        {
            get => firstName_;
            set => SetProperty(ref firstName_, value);
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        public event PropertyChangingEventHandler? PropertyChanging;

        protected void OnPropertyChanged(string? propertyName) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        protected bool SetProperty<T>(ref T field, T value, [CallerMemberName]string? propertyName = default)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;

            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
    }
}