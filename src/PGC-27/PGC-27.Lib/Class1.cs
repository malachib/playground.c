﻿using Microsoft.Extensions.Options;
using System;

namespace PGC_27.Lib
{
    /// <summary>
    /// Configuration POCO
    /// </summary>
    public class Class1
    {
        public string Value1 { get; set; }

        public int Value2 { get; set; }
    }
}
