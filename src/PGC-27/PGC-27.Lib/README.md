﻿# Outstanding questions

1. Do we really never (internally) cache the result and start with a fresh entity each time with OnChange chain?
A. Turns out there's a 3rd possibility.  Cache is used for scenarios where no change occurred.

# Guidance from:

1. https://andrewlock.net/simplifying-dependency-injection-for-iconfigureoptions-with-the-configureoptions-helper/

# Source Code

https://github.com/aspnet/Options/tree/master/src/Microsoft.Extensions.Options (1.x - 2.x)
https://github.com/dotnet/runtime/tree/main/src/libraries/Microsoft.Extensions.Options/src (5.x+)

# Wisdom Gleaned

## From Microsoft.Extensions.Options source

For 2.0+ a proper cache mechanism is used to feed TOptions out through OptionsMonitor.
When cache expires, an OptionsFactory steps in and recreates the TOptions, including calling
ConfigureOptions.Configure

For 1.1 a more rudimentary lazy initializer is used.  This never expires, so no OptionsFactory
is used (or even present)

### OptionsMonitor

Transient makes sense for OptionsMonitor since other parties can hold on to TOptions themselves,
making OptionsMonitor primarily useful for stoking the creation and recreation of TOptions as
necessary

### ChangeToken

ChangeToken callback registration happens right when IOptionsMonitor<TOptions> is retrieved from IoC

It seems that once a change occurs, `HasChanged` stays true and change token is done.  This is definitely the case
for https://docs.microsoft.com/en-us/dotnet/api/microsoft.extensions.fileproviders.physical.pollingfilechangetoken.haschanged?view=dotnet-plat-ext-6.0
"Once true, the value will always be true. Change tokens should not re-used once expired. The caller should discard this instance once it sees HasChanged is true."