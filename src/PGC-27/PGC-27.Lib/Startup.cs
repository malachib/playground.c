﻿using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;

namespace PGC_27.Lib
{
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sc"></param>
        /// <remarks>
        /// Guidance from:
        /// https://programming.vip/docs/ioptions-ioptions-monitor-and-ioptions-snapshot.html
        /// </remarks>
        public void ConfigureServices(IServiceCollection sc)
        {
            sc.AddOptions();

            // Token source appears to require a manual configuration here
            sc.AddSingleton<IOptionsChangeTokenSource<Class1>, CustomChangeTokenSource>();

            // Explicit config here gains us use of IServiceProvider in _ConfigurationOptions1
            sc.AddTransient<IConfigureOptions<Class1>, _ConfigureOptions1>();
            sc.Configure<Class1>(o => o.Value1 += ", Modified again");

            //sc.AddSingleton<IConfigureOptions<Class1>, _ConfigureOptions2>();
        }


        public void Configure(IServiceProvider services)
        {
            /*
            ChangeToken.OnChange(() => new CustomChangeToken(), () =>
            {
                Console.WriteLine("Got here");
            }); */
        }
    }
}
