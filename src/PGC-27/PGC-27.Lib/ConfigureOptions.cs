﻿using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGC_27.Lib
{
    public class _ConfigureOptions1 : IConfigureOptions<Class1>
    {
        readonly IServiceProvider services;

        public _ConfigureOptions1(IServiceProvider services)
        {
            // Just grabbing this to prove that we do get DI'd properly
            this.services = services;
        }

        public void Configure(Class1 options)
        {
            options.Value1 += "Updated by _ConfigureOptions1";
            options.Value2 += 2;
        }
    }

    // May not be quite as useful, concrete ConfigureOptions may just be used
    // to feed services.Configure(delegate) extension method
    // https://henkmollema.github.io/advanced-options-configuration-in-asp.net-core/
    public class _ConfigureOptions2 : ConfigureOptions<Class1>
    {
        public _ConfigureOptions2(Action<Class1> action) : base(action)
        {

        }
    }


    /// <summary>
    /// Proof of concept for a fully externally activated/manual change token
    /// </summary>
    public class ManualChangeToken : IChangeToken
    {
        event Action callbacks;
        LinkedList<Callback> callbacks2 = new LinkedList<Callback>();

        public bool HasChanged { get; private set; }

        //public bool ActiveChangeCallbacks => callbacks.GetInvocationList().Any();

        /// <summary>
        /// Indicates proactive callback raising, which we do
        /// </summary>
        public bool ActiveChangeCallbacks => true;

        public void Changed()
        {
            if (HasChanged) throw new InvalidOperationException("Token has already been changed");

            //var ctr = new ChangeTokenRegistration<int>
            HasChanged = true;

            // event-flavored version worked reasonably well, but perhaps
            // might result in too many events being registered over time?
            //callbacks?.Invoke();
            
            //lock (callbacks2)
            {
                // ToArray so that we snapshot since Invoke modifies this collection
                foreach (Callback callback in callbacks2.ToArray())
                    callback.Invoke();
            }

            // FIX: Unknown if this is handled right
            // It is implied based on PollingFileChangeToken that once HasChanged is true,
            // it stays that way and a new change token is needed.  This seems sensible.
            //HasChanged = false;
        }


        class Callback : IDisposable
        {
            readonly LinkedList<Callback> parent;
            readonly Action<object> callback;
            readonly object state;

            internal Callback(LinkedList<Callback> parent, Action<object> callback, object state)
            {
                this.parent = parent;
                this.callback = callback;
                this.state = state;
            }

            public void Invoke() => callback(state);

            public void Dispose()
            {
                parent.Remove(this);
            }
        }


        class Disposer : IDisposable
        {
            readonly Action onDispose;

            public Disposer(Action onDispose)
            {
                this.onDispose = onDispose;
            }

            public void Dispose()
            {
                onDispose?.Invoke();
                Console.WriteLine("Got here");
            }
        }

        public IDisposable RegisterChangeCallback(Action<object> callback, object state)
        {
            var c = new Callback(callbacks2, callback, state);

            // doesn't help, and would cause a deadlock anyway since this is called at the
            // end of the callback chain presumably to reregister
            //lock (callbacks2)

            {
                callbacks2.AddLast(c);
            }
            return c;
        }

        /// <summary>
        /// "Registers for a callback that will be invoked when the entry has changed"
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public IDisposable RegisterChangeCallbackOld(Action<object> callback, object state)
        {
            // "// Don't capture the current ExecutionContext and its AsyncLocals onto the token registration causing them to live forever"
            // https://github.com/dotnet/runtime/blob/edfed86a3b2b42fc9c802217cd00a1e4566216df/src/libraries/Microsoft.Extensions.Primitives/src/CancellationChangeToken.cs

            Action _callback = () => callback(state);
            callbacks += _callback;


            // DEBT: May not be necessary, never seems to be called
            return new Disposer(() => callbacks -= _callback);
        }
    }


    public class CustomChangeTokenSource : IOptionsChangeTokenSource<Class1>
    {
        /// <summary>
        /// Named configuration source parameter - make sure this is string.Empty
        /// for NET46 compatibility
        /// </summary>
        /// <remarks>
        /// Not used in NET46
        /// </remarks>
        public string Name => string.Empty;

        ManualChangeToken changeToken = new ManualChangeToken();

        public IChangeToken GetChangeToken()
        {
            // This singular return almost works, because IOptions system auto propagates
            // The nuance here is that ManualChangeToken is quite synthetic - multiple Change()
            // calls across all active ManualChangeTokens would be required in order to really
            // do this always-new thing here.  That said, production systems are likely to
            // behave more that way
            //return new ManualChangeToken();

            if (changeToken.HasChanged) changeToken = new ManualChangeToken();
            return changeToken;
        }
    }
}
