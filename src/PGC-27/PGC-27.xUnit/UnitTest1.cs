using Microsoft.Extensions.DependencyInjection;

using System;

using FluentAssertions;
using Xunit;

namespace PGC_27.xUnit
{
    using Lib;
    using Microsoft.Extensions.Options;
    using Microsoft.Extensions.Primitives;

    public class UnitTest1 : IClassFixture<Fixture>
    {
        [Fact]
        public void IOptionsTest()
        {
            var sc = new ServiceCollection();
            var s = new Startup();

            s.ConfigureServices(sc);

            var services = sc.BuildServiceProvider();

            s.Configure(services);

            var c1o = services.GetRequiredService<IOptions<Class1>>();

            var c = c1o.Value;

            c.Value2.Should().Be(2);
        }


        [Fact]
        public void IOptionsMonitorTest1()
        {
            var sc = new ServiceCollection();
            var s = new Startup();

            s.ConfigureServices(sc);

            var services = sc.BuildServiceProvider();

            s.Configure(services);

            var c1o = services.GetRequiredService<IOptionsMonitor<Class1>>();

            var v = c1o.CurrentValue;

            var test = services.GetRequiredService<IConfigureOptions<Class1>>();

            var test2 = services.GetRequiredService<IOptionsChangeTokenSource<Class1>>();

#if NET5_0
            var cache = services.GetRequiredService<IOptionsMonitorCache<Class1>>();
            var factory = services.GetRequiredService<IOptionsFactory<Class1>>();

            // Just to see what it does.  Indeed, Configure is called
            var factoryCreated = factory.Create(string.Empty);
#endif

            var ct = (ManualChangeToken)test2.GetChangeToken();
            //var testEntity = new Class1() { Value1 = "from xUnit" };

            int counter = 0;

            var disposable = c1o.OnChange(c =>
            {
                Console.WriteLine("Got here");

                // Noteworthy, old 'c' never seems to get cached
                c.Value2.Should().Be(2);
                ++counter;
            });

            // Indicates that underlying store that IConfigureOptions<Class1> is looking at
            // has changed.  Be aware system treats this as a fresh start, each
            // call to _ConfigureOptions1::Configure has nulled settings
            ct.Changed();

            ct = (ManualChangeToken)test2.GetChangeToken();
            ct.Changed();

            disposable.Dispose();
        }

        [Fact]
        public void IOptionsMonitorTest2()
        {
            var sc = new ServiceCollection();
            var s = new Startup();

            s.ConfigureServices(sc);

            var services = sc.BuildServiceProvider();

            s.Configure(services);

            var c1o = services.GetRequiredService<IOptionsMonitor<Class1>>();
            var octs = services.GetRequiredService<IOptionsChangeTokenSource<Class1>>();
            var ct = (ManualChangeToken)octs.GetChangeToken();

            int counter = 0;

            using (var d = ct.RegisterChangeCallback(o => ++counter, null))
            {
                ct.Changed();
            }

            ct = (ManualChangeToken)octs.GetChangeToken();
        }
    }
}
