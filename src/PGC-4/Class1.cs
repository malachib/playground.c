﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGC_4
{
    // Using https://msdn.microsoft.com/en-us/magazine/mt707534.aspx as reference
    public class Class1
    {
        readonly ILogger logger;
        readonly IServiceProvider services;

        public Class1(IServiceCollection serviceCollection)
        {
            ConfigureServices(serviceCollection);
            services = serviceCollection.BuildServiceProvider();
            logger = services.
                GetRequiredService<ILoggerFactory>().
                CreateLogger<Class1>();

            logger.LogInformation("Class1 created successfully");
        }

        static void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IPaymentService, PaymentService>();
        }

        public void MakePayment(float amount)
        {
            logger.LogInformation(
              $"Begin making a payment {amount}");
            var paymentService = services.GetRequiredService<IPaymentService>();
        }
    }

    public interface IPaymentService
    { }

    public class PaymentService : IPaymentService
    {
        public ILogger Logger { get; }
        public PaymentService(ILogger<PaymentService> _logger)
        {
            Logger = _logger;
            if (Logger == null)
            {
                throw new ArgumentNullException(nameof(_logger));
            }
            Logger.LogInformation("PaymentService created");
        }
    }
}
