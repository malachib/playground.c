﻿#if NETCORE
using PGC_4;
#endif
using System;

namespace Console__net.core_
{
    public class Program
    {
        public void Main(string[] args)
        {
#if NETCORE
            var cl = new Class1();
            cl.TestFunc();
#endif
            Console.WriteLine("Hello World");
            Console.ReadLine();
        }
    }
}
