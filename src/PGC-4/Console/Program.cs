﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var test = new PGC_4.Class1(serviceCollection);

            test.MakePayment(5);

            System.Console.WriteLine("Press enter");
            System.Console.ReadLine();
        }

        static void ConfigureServices(IServiceCollection serviceCollection)
        {
            ILoggerFactory loggerFactory = new LoggerFactory();
            /*
            var sd = new ServiceDescriptor(typeof(ILogger), sp =>
            {
                return null;
            }, ServiceLifetime.Singleton);*/
            
            serviceCollection.AddSingleton(loggerFactory);
            serviceCollection.TryAdd(ServiceDescriptor.Singleton(typeof(ILogger<>), typeof(Logger<>)));

            loggerFactory.AddConsole();
        }
    }
}
