﻿using Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PGC_26.WinForms
{
    public partial class Form1 : Form
    {
        Class1 class1 = new Class1();
        Class2 class2 = new Class2();

        public Form1()
        {
            InitializeComponent();

            class2.UiResponderEvent += class1.UI_Responder;
            class1.DoStuffDone += Class1_DoStuffDone;

            txtMainThreadId.Text = Thread.CurrentThread.ManagedThreadId.ToString();
        }

        private void Class1_DoStuffDone(int arg1, int arg2)
        {
            txtCallingThreadId.Text = arg1.ToString();
            txtExceptionThreadId.Text = arg2.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtCallingThreadId.Text = "";
            txtExceptionThreadId.Text = "";

            class2.FireUiResponderEvent();
        }
    }
}
