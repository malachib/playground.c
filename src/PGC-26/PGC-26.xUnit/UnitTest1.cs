using System;
using System.Threading;
using System.Threading.Tasks;
using Core;
using FluentAssertions;
using Xunit;

namespace xUnit
{
    public class UnitTest1
    {
        [Fact]
        public async Task Test1()
        {
            var c = new Class1();

            await Assert.ThrowsAsync<InvalidOperationException>(() => c.DoStuff1());
        }


        [Fact]
        public async Task Test2()
        {
            var id = Thread.CurrentThread.ManagedThreadId;
            var c1 = new Class1();
            var c2 = new Class2();
            var tcs = new TaskCompletionSource<int>();
            int mainThreadId = -1;

            c1.DoStuffDone += (_mainThreadId, exceptionThreadId) =>
            {
                mainThreadId = _mainThreadId;
                tcs.SetResult(exceptionThreadId);
            };
            c2.UiResponderEvent += c1.UI_Responder;

            c2.FireUiResponderEvent();

            int exceptionThreadId = await tcs.Task;

            id.Should().Be(mainThreadId);
            id.Should().Be(exceptionThreadId, "hoping exception and main thread Id are the same");
        }
    }
}