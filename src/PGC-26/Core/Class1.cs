﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Core
{
    public class Class1
    {
        /// <summary>
        /// Fires with paremeters:
        /// - first int = pre-exception managed thread ID
        /// - second int = managed thread ID during exception
        /// </summary>
        public event Action<int, int> DoStuffDone;

        /// <summary>
        /// Do some arbitrary slow work then throw an exception
        /// </summary>
        /// <returns></returns>
        public async Task DoStuff1()
        {
            await Task.Delay(1000);

            throw new InvalidOperationException();
        }

        public async void UI_Responder()
        {
            int mainId = -1;

            try
            {
                var context = SynchronizationContext.Current;
                mainId = Thread.CurrentThread.ManagedThreadId;
                await DoStuff1();
            }
            catch(InvalidOperationException)
            {
                var context = SynchronizationContext.Current;
                // When invoked directly from C++/CLI via Class2::FireUiResponderEvent()
                // We see id = 4 here sometimes.
                var id = Thread.CurrentThread.ManagedThreadId;
                DoStuffDone?.Invoke(mainId, id);
            }
        }
    }


    public class Class2
    {
        public event Action UiResponderEvent;

        public void FireUiResponderEvent() =>
            UiResponderEvent?.Invoke();

        public void FireUiResponderEventOnNewThread() =>
            Task.Run(FireUiResponderEvent);

        public void Setup(Class1 c1)
        {
            UiResponderEvent += c1.UI_Responder;
        }
    }
}