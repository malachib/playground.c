Looking like during exception processing:

* WinForms-native sync context always retains Thread ID
* sync context used by xUnit or MFC worker thread never retains Thread ID
* C++/CLI requires manual SetSynchronizationContext

## C++/CLI MFC

### If SetSynchronizationContext is not called

Observe that if one runs "Test2" straight from the beginning, divergent Thread IDs are seen
Observe that if one runs "Test1", "Test3" or "Test5", further "Test2" Thread IDs align

`SynchronizationContext.Current == null` until Test1 or Test3 is run, at which point it is WindowsFormsSynchronizationContext

### If SetSynchronizationContext is called

All operations perform as expected

### Additional Thoughts

Since WindowsFormsSynchronizationContext only has one constructor with no parameters, we can infer it's pretty
safe for us to manually assign it - no magic setup of this object is expected

### Conclusions

C++/CLI MFC does not proactively set a sync context, which makes some sense since even in a managed C++/CLI
context they would not presume that you'd be using WinForms.

Any time a WinForms activity directly happens such as popping up a WinForms Form or even a hosted WinForms
contnrol in an MFC dialog, sync context is automatically set up.