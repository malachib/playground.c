// 07DEC21 MB Hand-made, not via a wizard
// Guidance from:
// https://docs.microsoft.com/en-us/cpp/dotnet/how-to-create-the-user-control-and-host-in-a-dialog-box?view=msvc-170

#include "pch.h"
#include "CDialog1.h"

#include "framework.h"
#include "afxwinappex.h"
#include "afxdialogex.h"

CDialog1::CDialog1() noexcept : CDialogEx(IDD_DIALOG1)
{
}


void CDialog1::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_ManagedControl(pDX, IDC_CTRL1, m_ctrl1);
}

BEGIN_MESSAGE_MAP(CDialog1, CDialogEx)
END_MESSAGE_MAP()
