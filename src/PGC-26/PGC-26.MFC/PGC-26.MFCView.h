
// PGC-26.MFCView.h : interface of the CPGC26MFCView class
//

#pragma once


class CPGC26MFCView : public CView
{
protected: // create from serialization only
	CPGC26MFCView() noexcept;
	DECLARE_DYNCREATE(CPGC26MFCView)

// Attributes
public:
	CPGC26MFCDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CPGC26MFCView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PGC-26.MFCView.cpp
inline CPGC26MFCDoc* CPGC26MFCView::GetDocument() const
   { return reinterpret_cast<CPGC26MFCDoc*>(m_pDocument); }
#endif

