
// PGC-26.MFCView.cpp : implementation of the CPGC26MFCView class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "PGC-26.MFC.h"
#endif

#include "PGC-26.MFCDoc.h"
#include "PGC-26.MFCView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPGC26MFCView

IMPLEMENT_DYNCREATE(CPGC26MFCView, CView)

BEGIN_MESSAGE_MAP(CPGC26MFCView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CPGC26MFCView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CPGC26MFCView construction/destruction

CPGC26MFCView::CPGC26MFCView() noexcept
{
	// TODO: add construction code here

}

CPGC26MFCView::~CPGC26MFCView()
{
}

BOOL CPGC26MFCView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CPGC26MFCView drawing

void CPGC26MFCView::OnDraw(CDC* /*pDC*/)
{
	CPGC26MFCDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CPGC26MFCView printing


void CPGC26MFCView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CPGC26MFCView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CPGC26MFCView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CPGC26MFCView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CPGC26MFCView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CPGC26MFCView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CPGC26MFCView diagnostics

#ifdef _DEBUG
void CPGC26MFCView::AssertValid() const
{
	CView::AssertValid();
}

void CPGC26MFCView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CPGC26MFCDoc* CPGC26MFCView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPGC26MFCDoc)));
	return (CPGC26MFCDoc*)m_pDocument;
}
#endif //_DEBUG


// CPGC26MFCView message handlers
