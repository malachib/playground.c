
// PGC-26.MFC.h : main header file for the PGC-26.MFC application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CPGC26MFCApp:
// See PGC-26.MFC.cpp for the implementation of this class
//

class CPGC26MFCApp : public CWinAppEx
{
public:
	CPGC26MFCApp() noexcept;


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	UINT  m_nAppLook;
	BOOL  m_bHiColorIcons;

	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();

	afx_msg void OnAppAbout();

	afx_msg void OnTest1();
	afx_msg void OnTest2();
	afx_msg void OnTest3();
	afx_msg void OnTest4();
	afx_msg void OnTest5();

	DECLARE_MESSAGE_MAP()
	static void OnDoStuffDone(int arg1, int arg2);
};

extern CPGC26MFCApp theApp;
