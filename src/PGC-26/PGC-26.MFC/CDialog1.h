#pragma once

#include "resource.h"       // main symbols
#include <afxwinforms.h>

class CDialog1 : public CDialogEx
{
	CWinFormsControl<PGC_26::WinForms::UserControl1> m_ctrl1;

public:
	CDialog1() noexcept;

	// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

