﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace swagger_autorest.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpGet("geta/{a}")]
        [Produces("application/json", Type = typeof(string))]
        public IActionResult GetA(string a)
        {
            return Json(a);
        }

        [HttpGet("getb/{b}")]
        [Produces("application/json", Type = typeof(string))]
        public IActionResult GetB(string b)
        {
            return Json(b);
        }

        [HttpGet("get_test_class")]
        [SwaggerOperation(operationId: "get_test_class")]
        [Produces("application/json")]
        public IActionResult GetTestClass()
        {
            // Interestingly, this ends up being item1/item2
            return Json((name: "Bob", age: 42));
        }

        public class PostModel
        {
            public string Id {get;set;}
            public string Title {get;set;}
        }

        public class PostModelExtended : PostModel
        {
            public string FavoriteColor {get;set;}
        }

        /// <summary>
        /// API to get a dummy blog bost
        /// </summary>
        /// <param name="id">unique id of post</param>
        /// <returns></returns>
        [HttpGet("get_post_by_id/{id}")]
        [Produces("application/json", Type = typeof(PostModel))]
        [SwaggerOperation(operationId: "getBlogPostById")]
        [ProducesResponseType(typeof(PostModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(PostModelExtended), 210)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)] // will return a null instead of a 404
        public IActionResult GetPostById(string id)
        {
            var _cache = new PostModel[] {
                new PostModel() { Id = "1", Title = "Title #1" },
                new PostModelExtended() { Id = "2", Title = "Colorful", FavoriteColor = "Red" }
            };
            var post = _cache.FirstOrDefault(p => p.Id.Equals(id, StringComparison.OrdinalIgnoreCase));
            if (post == null)
            {
                return NotFound();
            }

            var result = Json(post);

            // we DO have to explicitly state a 210 is coming back, remember
            // ProducesResponseType is just a metadata helper
            if(post is PostModelExtended) result.StatusCode = 210;

            return result;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
