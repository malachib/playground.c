﻿using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App1
{
    public partial class App : Application
    {
        public static PublicClientApplication IdentityClientApp = null;
        public static UIParent UiParent = null;

        // AD application identifier and request permissions, or scopes
        public static string ClientID = "cdc711d8-8887-4c13-8595-26aa9a152ddf";
        public static string[] Scopes = { "User.Read", "User.ReadBasic.All", "Mail.Send" };

        public App()
        {
            InitializeComponent();

            IdentityClientApp = new PublicClientApplication(ClientID);

            MainPage = new App1.MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
