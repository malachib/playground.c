﻿using Microsoft.Graph;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    public partial class MainPage : ContentPage
    {
        // Cobbled together myself
        GraphServiceClient Client;
        User Me;

        public MainPage()
        {
            InitializeComponent();
        }

        private async Task<bool> CreateGraphClientAsync()
        {
            try
            {
                Client = new GraphServiceClient("https://graph.microsoft.com/v1.0",
                      new DelegateAuthenticationProvider(async(requestMessage) =>
                {
                    var tokenRequest = await App.IdentityClientApp.AcquireTokenAsync(App.Scopes, App.UiParent).ConfigureAwait(false);
                    requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", tokenRequest.AccessToken);
                }));
                Me = await Client.Me.Request().GetAsync();
                Username.Text = $"Welcome {((User)Me).DisplayName}";
                return true;
            }
            catch (MsalException ex)
            {
                await DisplayAlert("Error", ex.Message, "OK", "Cancel");
                return false;
            }
        }

#if UNUSED
        private async void SendEmail(Message message)
        {
            if (!UserExists)
                await CreateGraphClientAsync();
            var req = Client.Me.SendMail(message);
            await req.Request().PostAsync();
            Status.Text = $"Email sent to your manager { ((User)Manager).DisplayName }, CC: you";
        }
#endif
    }
}
