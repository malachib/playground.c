﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Blazor;
using Microsoft.AspNetCore.Blazor.Components;
//using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace POC1
{
    // TODO: Pretty sure this was a very experimental approach (RazorPage)
    // and probably now we should be using BlazorComponent, as indicated
    // here https://blazor.net/docs/components/index.html
    // TODO: DI thru blazor is done with [Inject] attribute, so use that
    //public class TestIndexPage<TModel> : RazorPage<TModel>
    public class TestIndexPage<TModel> : BlazorComponent
    {
        readonly ILogger logger;

        public string WelcomeText => "Let's get started";

        public TestIndexPage()
        {
            logger = Program.ServiceLocator.GetService<ILogger<TestIndexPage<TModel>>>();

            logger.LogDebug("ctor");
        }

        /* Does not work
        public TestIndexPage(ILogger logger)
        {
            this.logger = logger;
        } */
    }


    public class FakeRepository
    {
        readonly ILogger logger;

        public void DummyFunc()
        {
            logger.LogInformation("DummyFunc called");
        }

        public FakeRepository(ILogger<FakeRepository> logger)
        {
            this.logger = logger;
        }
    }
}
