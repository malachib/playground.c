﻿using Microsoft.AspNetCore.Blazor.Browser.Rendering;
using Microsoft.AspNetCore.Blazor.Browser.Services;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using System;

namespace POC1
{
    class Program
    {
        public static IServiceProvider ServiceLocator { get; private set; }

        static void Main(string[] args)
        {
            var serviceProvider = new BrowserServiceProvider(configure =>
            {
                // Add any custom services here
                configure.AddLogging(lb =>
                {
                    // Console logger crashes things because it depends on threading which
                    // WASM apparently does not support
                    //lb.AddConsole();  // This seems to crash things right away (on FakeRepository DI)
                    lb.AddDebug(); // this doesn't crash anything, but doesn't appear to link into any browser console output either
                    lb.AddProvider(new SimpleConsoleLoggerProvider());

                    lb.SetMinimumLevel(LogLevel.Trace);
                });

                configure.AddSingleton<FakeRepository>();
            });

            ServiceLocator = serviceProvider;

            new BrowserRenderer(serviceProvider).AddComponent<App>("app");
        }
    }
}
