﻿using Microsoft.AspNetCore.Blazor.Components;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POC1
{
    public class TestCounterPage : BlazorComponent
    {
        // At time of writing:
        // Recommended DI approach as per https://learn-blazor.com/architecture/view-logic-separation/
        // Constructor DI not working
        // Expected these will both change in the future
        [Inject]
        protected ILogger<TestCounterPage> Logger { get; set; }
    }
}
