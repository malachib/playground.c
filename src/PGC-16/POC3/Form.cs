﻿using Microsoft.AspNetCore.Blazor;
using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace POC3
{
    public class FormRecord : INotifyPropertyChanged
    {
        void DoPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        string firstName;

        public string FirstName
        {
            get => firstName;
            set
            {
                firstName = value;
                DoPropertyChanged();
            }
        }
        public string Email { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    /// <summary>
    /// TODO: Did a lot of experimentation, but still have yet to check how much of
    /// standard Razor validation model applies  (see Bitbucket task for link)
    /// </summary>
    public class Form : RazorPage<FormRecord>, INotifyPropertyChanged
    {
        // Not quite sure how to access RazorPage<FormRecord> model, if at all, so
        // making explicit one here

        public FormRecord Record { get; set; } = new FormRecord();

        public string Title { get; set; } = "POC3 validation test";

        public Form()
        {
            Record.PropertyChanged += (s, e) =>
            {
                // Looks like reflection is fully functional in Blazor, that's good
                var p = s.GetType().GetProperty(e.PropertyName);
                Console.WriteLine($"Record.{e.PropertyName} changed to: {p.GetValue(s)}");
            };
            Console.WriteLine("Form created");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // handlers gleaned from https://codedaze.io/blazor-bites-data-binding-and-events/

        // NOTE: interestingly, doesn't get called on first assignment -
        // presumably the null -> value change doesn't trigger an onchange
        // NOTE: Also, only is called on lose focus, not per keypress
        protected void OnChange(object newValue)
        {
            // utilizing this really does eat up inbuilt data binding
            Console.WriteLine($"Changing value={newValue}");
        }

        // TODO: Find a way for this to interact with binding mechanism, so that we can
        // keep binding but also push through keys into our validation handler
        protected void OnKeyPress(UIEventArgs eventArgs)
        {
            var e = (UIKeyboardEventArgs)eventArgs;
            Console.WriteLine($"OnKeyPress Key={e.Key}");
        }
    }
}
