using Microsoft.EntityFrameworkCore;

namespace Blazor1.Server
{
    public class ToDoListContext : DbContext
    {
        public ToDoListContext(DbContextOptions<ToDoListContext> options) : base(options) { }

        public DbSet<ToDoList> ToDoLists { get; set; }
    }
}