﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace AES.lib
{
    public class Method1
    {
        public void test1(byte[] key, byte[] IV)
        {
            // "The Rijndael and RijndaelManaged types are obsolete. Use Aes instead."
            // https://learn.microsoft.com/en-us/dotnet/api/system.security.cryptography.rijndaelmanaged?view=net-7.0
            var aes = new RijndaelManaged();
        }
    }


    public class Method2
    {
        void test1()
        {
            var aes = new AesManaged();
        }

        public byte[] encrypt2(byte[] key, byte[] IV, ReadOnlySpan<byte> toEncrypt)
        {
            using Aes aes = Aes.Create();
            aes.Key = key;
            aes.IV = IV;

            ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

            using var ms = new MemoryStream();
            using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
            {
                cs.Write(toEncrypt);
            }

            return ms.ToArray();
        }

        // DEBT: Would really like toDecrypt to be a ReadOnlySpan
        public void decrypt2(byte[] key, byte[] iv, Stream input, Stream output)
        {
            using Aes aes = Aes.Create();
            aes.Key = key;
            aes.IV = iv;

            ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

            using var cs = new CryptoStream(input, decryptor, CryptoStreamMode.Read);

            cs.CopyTo(output);
        }

        
        public async Task decrypt2Async(byte[] key, byte[] iv, Stream input, Stream output)
        {
            using Aes aes = Aes.Create();
            aes.Key = key;
            aes.IV = iv;

            // DEBT: Research why we must specify key & iv twice
            ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
            
            await using var cs = new CryptoStream(input, decryptor, CryptoStreamMode.Read);

            await cs.CopyToAsync(output);;
        }
        
        public byte[] decrypt2(byte[] key, byte[] iv, byte[] toDecrypt)
        {
            using var input = new MemoryStream(toDecrypt);
            using var output = new MemoryStream();

            decrypt2Async(key, iv, input, output).Wait();

            return output.ToArray();
        }
    }
}