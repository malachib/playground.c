# Overview

This document confirms to [RFC 7112](https://datatracker.ietf.org/doc/html/rfc2119) requirement levels

Document v0.1

## Notes

AES IVs MUST be random and unique (non repeating).[^1]
AES IVs need not be secret (MAY be plaintext viewable).[^2]

This type of number is also known as a **nonce**[^3]

# References

[^1]: https://csrc.nist.gov/glossary/term/initialization_vector
[^2]: https://security.stackexchange.com/questions/17044/when-using-aes-and-cbc-is-it-necessary-to-keep-the-iv-secret
[^3]: https://en.wikipedia.org/wiki/Cryptographic_nonce