using System.Security.Cryptography;
using AES.lib;
using FluentAssertions;
using System.Text;
using System.Text.Encodings;

namespace AES.xUnit;

public class UnitTest1
{
    //private static byte[] key1 = { 0, 1, 2, 3 };
    //private static byte[] iv1 = { 4, 5, 6, 7 };
    
    [Fact]
    public void Test1()
    {
        var m1 = new Method2();
        const string original = "Hello";

        byte[] key, iv, iv2;

        using (Aes aes = Aes.Create())
        {
            key = aes.Key;
            iv = aes.IV;
            
            aes.GenerateIV();

            iv2 = aes.IV;
        }

        byte[] encrypted = m1.encrypt2(key, iv, Encoding.ASCII.GetBytes(original));
        byte[] decrypted = m1.decrypt2(key, iv, encrypted);

        string compared = Encoding.ASCII.GetString(decrypted);

        compared.Should().BeEquivalentTo(original);
    }
}