﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Policy;
using System.IdentityModel.Claims;
using NLog;

namespace PGC_1
{
    public class AuthorizationPolicy : IAuthorizationPolicy
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        static void crasher()
        {
            throw new Exception(); 
        }

        static AuthorizationPolicy()
        {
            logger.Info("Static constructor: enter");

            try
            {
                Parallel.Invoke(crasher, crasher, crasher);
            }
            catch(Exception e)
            {
                logger.Error("Init exception: " + e.Message, e);
            }

            logger.Info("Static constructor: exit");
        }

        public string Id { get { return "TEST"; } }

        public ClaimSet Issuer
        {
            get { return ClaimSet.System; }
        }

        public bool Evaluate(EvaluationContext evaluationContext, ref object state)
        {
            return true;
        }
    }
}
