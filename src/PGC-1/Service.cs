﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace PGC_1
{
    public class Service : IService1
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        static Service()
        {
            logger.Info("Static constructor enter");
        }

        public string GetData(int value)
        {
            return "Data: " + value;
        }
    }
}
