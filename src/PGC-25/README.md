﻿# PGC-25 Overall NuGet + CSPROJ package and version testing

## Overall Findings

### Embedding sources

<EmbedAllSources> tag appears to be ignored by VS2017 15.9.x and VS Mac 8.4.2

On VSMac, even when using `dotnet pack --include-source` the included source in symbols.nupkg appears
to be ignored

## poc1

Combined VS2015 and VS2017, VS2019 testing
This *might* be skewing results with caching, etc. so splitting things out now

### VS2015

No combination yields available NuGet's for VS2015 feed.  Maybe because dotnet pack /
nuget being used is too new?  Things tried which made no difference:

1.  Using DebugType of `full` instead of `portable`

## poc2

VS2017 only: snupkg style

Excluded Producer from build, to avoid accidental confusing recompilations

### Findings

Acts like it's only able to debug when NuGet'd DLL matches exactly the current
source

## poc3

VS2017 only: symbol.nupkg style

## poc4

VS2019 only - deprioritized, as just about any VS2017 solution is expected to work here

## Project Guidance

Beware those nupkg's aren't checked in, you'll need to build them manually
via this Producer project for the hard-coded Consumer's NuGet.Config to find things

To perform test:

1. compile unchanged and ensure `UnitTest1.cs` runs properly.
2. modify Class1.cs in some visible way.  Recompile
3. do not update or recompile `UnitTest.cs`.  Debug `UnitTest.cs`
4. observe that recompiled Class1.cs is *not* reflected during debug, since we're using symbols from 
   snupkg



## General reference:

https://docs.microsoft.com/en-us/nuget/create-packages/creating-a-package-msbuild
https://docs.microsoft.com/en-us/nuget/create-packages/symbol-packages-snupkg

## Reference for really stuffing all the goods into the symbol area:

https://stackoverflow.com/questions/55736447/how-to-package-and-deploy-a-nuget-package-with-symbols-and-source-code-so-that-d

## Good general guidance for versioning:

https://andrewlock.net/version-vs-versionsuffix-vs-packageversion-what-do-they-all-mean/

## Can't confirm:

This article states VS2017 can do all the .snupkg trickery, however I am unable to confirm this
https://devblogs.microsoft.com/nuget/improved-package-debugging-experience-with-the-nuget-org-symbol-server/
