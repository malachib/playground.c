﻿using System;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Hello World! {Producer.Class1.Testable()}");
            Console.ReadKey();
        }
    }
}
