﻿using System;
using System.Reflection;

namespace Producer
{
    public class Class1
    {
        public static string Testable() => "hi2u.poc2.alpha.4 " + 
            Assembly.GetExecutingAssembly().GetName().Version;
    }
}
