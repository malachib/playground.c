﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Routing.Controllers
{
    public class TestController : Controller
    {
        public static int counter { get; set; } = 0;

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.counter = counter++;
            return View();
        }

        public IActionResult Test1(int id)
        {
            var url = Url.Action(nameof(Index));
            return Content($"Go check out {url}, it's really great.");
        }
    }


    public class TestModel
    {
        public string FirstName { get; set; }
    }
}
