﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PGC_6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// <remarks>
    /// http://www.codeproject.com/Articles/683429/Guide-to-WPF-DataGrid-formatting-using-bindings
    /// </remarks>
    public partial class MainWindow : Window
    {
        public class ColumnItem
        {
            public string Name { get; set; }
            public bool IsGreen { get; set; }
            public bool IsRed { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }
        public class RowItem
        {
            public ColumnItem[] Column { get; set; }
        }
        public MainWindow()
        {
            InitializeComponent();

            var rows = new RowItem[]
            {
                new RowItem()
                {
                    Column = new ColumnItem[] 
                    {
                        new ColumnItem() { Name = "Item 0x0" },
                        new ColumnItem() { Name = "Item 1x0", IsGreen = true },
                        new ColumnItem() { Name = "Item 2x0" },
                    }
                },
                new RowItem()
                {
                    Column = new ColumnItem[]
                    {
                        new ColumnItem() { Name = "Item 0x1" },
                        new ColumnItem() { Name = "Item 1x1" },
                        new ColumnItem() { Name = "Item 2x1", IsRed = true },
                    }
                },
                new RowItem()
                {
                    Column = new ColumnItem[]
                    {
                        new ColumnItem() { Name = "Item 0x2", IsRed = true },
                        new ColumnItem() { Name = "Item 1x2", IsRed = true },
                        new ColumnItem() { Name = "Item 2x2" },
                    }
                }
            };

            DataContext = rows;
        }
    }

    public class TestConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
