﻿using System;
using System.Linq;

namespace ConsoleApp.SQLite
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new BloggingContext())
            {
                db.Blogs.Add(new Blog { Url = "http://blogs.msdn.com/adonet" });
                var count = db.SaveChanges();
                Console.WriteLine("{0} records saved to database", count);

                Console.WriteLine();
                Console.WriteLine("All blogs in database:");

                var blogScores = db.BlogScores.ToArray();

                int counter = 0;

                foreach (var blog in db.Blogs)
                {
                    Console.WriteLine(" - {0}", blog.Url);
                    var bs = new BlogScore();

                    bs.Score = counter++;
                    bs.Blog = blog;
                    db.BlogScores.Add(bs);
                }

                //db.SaveChanges();
            }
        }
    }
}
