using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using System.Json;
//using Microsoft.AspNet.Builder;
//using Microsoft.AspNet.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

//using Microsoft.Extensions.Json;
//using Microsoft.AspNet.Authentication.OAuth;

using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Hosting;

namespace GitHubAuthApp
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if(env.IsDevelopment())
            {
                builder.AddUserSecrets();
            }

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFramework()
              .AddSqlServer()
              .AddDbContext<ApplicationDbContext>(options =>
                  options.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if(env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {

            }

            app.UseIISPlatformHandler();

            app.UseStaticFiles();

            app.UseIdentity();
            app.UseOAuthOptions(GitHubOptions);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"
                );
            });
        }

        private OAuthOptions GitHubOptions =>

            new OAuthOptions
            {
                AuthenticationScheme = "GitHub",
                DisplayName = "GitHub",
                ClientId = Configuration["GitHub:ClientId"],
                ClientSecret = Configuration["GitHub:ClientSecret"],
                CallbackPath = new PathString("/signin-github"),
                AuthorizationEndpoint = "https://github.com/login/oauth/authorize",
                TokenEndpoint = "https://github.com/login/oauth/access_token",
                UserInformationEndpoint = "https://api.github.com/user",
                ClaimsIssuer = "OAuth2-Github",
                SaveTokensAsClaims = true,

                // Retrieving user information is unique to each provider.
                Events = new OAuthEvents
                {
                    OnCreatingTicket = async context => { await CreateGitHubAuthTicket(context); }
                }
            };

        private static async Task CreateGitHubAuthTicket(OAuthCreatingTicketContext context)
        {
            // Get the GitHub user
            var request = new HttpRequestMessage(HttpMethod.Get, context.Options.UserInformationEndpoint);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", context.AccessToken);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await context.Backchannel.SendAsync(request, context.HttpContext.RequestAborted);
            response.EnsureSuccessfulStatusCode();

            var user = JObject.Parse(await response.Context.ReadAsStringAsync());

            AddClaims(context, user);
        }

        private static void AddClaim<TValue>(OAuthCreatingTicketContext context, JObject user, string claimKey, string key)
        {
            var value = user.Value<string>(key);
            if(!string.IsNullOrEmpty(identifier))
            {
                context.Identity.AddClaim(new Claim(
                    claimKey, value,
                    ClaimValueTypes.String, context.Options.ClaimsIssuer
                ));
            }
        }

        private static void AddClaims(OAuthCreatingTicketContext context, JObject user)
        {
            addClaim<string>(context, user, ClaimTypes.NameIdentifier, "id");
          /*
            var identifier = user.Value<string>("id");
            if(!string.IsNullOrEmpty(identifier))
            {
                context.Identity.AddClaim(new Claim(
                    ClaimTypes.NameIdentifier, identifier,
                    ClaimValueTypes.String, context.Options.ClaimsIssuer
                ))
            } */
            addClaim<string>(context, user, ClaimIdentity.DefaultNameClaimType, "login");
            addClaim<string>(context, user, "urn:github:name", "name");
            addClaim<string>(context, user, "urn:github:name", "url");
        }
    }
}
