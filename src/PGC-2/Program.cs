﻿using Castle.Core.Logging;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGC_2
{
    class Program
    {
        static readonly IWindsorContainer container = new WindsorContainer();

        static void Main(string[] args)
        {
            container.Install(
                //FromAssembly.This,
                //FromAssembly.This
                new LoggingInstaller()
                );

            container.Register(Classes.
                FromThisAssembly().
                BasedOn<ITestClass>().
                LifestyleTransient());

            var factory = container.Resolve<ILoggerFactory>();

            var logger3 = factory.Create("default-y");
            //var _logger = container.Resolve<ILogger>("test2");
            var logger = container.Resolve<ILogger>();
            var logger2 = logger.CreateChildLogger("test");
            //_logger.Info("Using _logger");
            logger.Info("Using logger");
            logger2.Info("Using this logger also");
            logger3.Info("Using logger");

            var c = new TestClass();

            // as expected, this doesn't do what we'd like it to - just logs
            // to NULL
            c.DoStuff();

            var c2 = container.Resolve<TestClass>();

            c2.DoStuff();
        }
    }


    public interface ITestClass { }


    public class TestClass : ITestClass
    {
        public TestClass()
        {
            Logger = NullLogger.Instance;
        }

        public void DoStuff()
        {
            Logger.Info("Hi there");
        }

        public ILogger Logger { get; set; }
    }

    public class LoggingInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<LoggingFacility>(m => m.UseNLog().WithConfig("NLog.config"));
        }
    }
}
