﻿using System;

namespace PGC_3._1
{
    public class Program
    {
        public void Main(string[] args)
        {
            // OK, so dependencies implement a superset of what consuming assemblies want
            // in this case, aspNetLib1 can implement ASP.NET 5.0 *and* ASP.NET 5.0 Core
            // while aspNetLib2 only implements ASP.NET 5.0 Core - and since PGC_3._1
            // only "demands" ASP.NET 5.0 Core, it can use both.  Unlike PGC_3 which
            // "demands" ASP.NET 5.0 *and* ASP.NET 5.0 Core
            //
            // Interestingly, even though aspNetLib1 doesn't actually *USE* ASP.NET 5.0
            // just referencing it as a framework is enough to "mark" it.  Fair enough
            var c = new aspNetLib1.Class1();
            var c2 = new aspNetLib2.Class1(); 

            Console.WriteLine("Hello World");
            Console.ReadLine();
        }
    }
}
