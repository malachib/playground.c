﻿using System;
using System.Threading.Tasks;

namespace aspNetLib1
{
    public class Class1
    {
        public Class1()
        {
            Parallel.Invoke(() =>
                Console.WriteLine("Did something in parallel (sorta)"));
        }
    }
}
