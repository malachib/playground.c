﻿using System;

namespace PGC_3
{
    public class Program
    {
        public void Main(string[] args)
        {
            var c = new aspNetLib1.Class1();

            // Unexpected that this doesn't work
            // The dependencies work in reverse from what I expected
            //var c2 = new aspNetLib2.Class1(); 

            Console.WriteLine("Hello World");
            Console.ReadLine();
        }
    }
}
